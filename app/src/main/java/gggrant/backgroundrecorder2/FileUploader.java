package gggrant.backgroundrecorder2;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.security.KeyStore;
import java.security.cert.Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;

public class FileUploader {

    public static String TAG = FileUploader.class.getSimpleName();

    public static void upload(String filepath) {

        try {

            Certificate ca = Res.getRawCertificate(R.raw.selfsigned);

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            File videoFile = new File(filepath);
            RequestParams params = new RequestParams();
            params.put("video", videoFile);

            AsyncHttpClient client = new AsyncHttpClient();

            String apiKey = Res.getRawString(R.raw.api_key);
            client.addHeader("apikey", apiKey);

            client.setSSLSocketFactory(
                    new SSLSocketFactory(context,
                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            client.post(Prefs.getUploadRoute(), params, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    Log.d(TAG, "onStart");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                    Log.d(TAG, "onSuccess");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                    Log.e(TAG, "onFailure");
                    //Log.e(TAG, statusCode + "!");
                    //Log.e(TAG, new String(errorResponse));
                }

                @Override
                public void onRetry(int retryNo) {
                    Log.e(TAG, "onRetry");
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}

