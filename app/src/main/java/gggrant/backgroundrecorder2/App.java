package gggrant.backgroundrecorder2;

import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Res.init(this);
        Prefs.init(this);
        CameraFactory.init(this);
        MediaRecorderFactory.init(this);
    }
}
