package gggrant.backgroundrecorder2;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.media.CamcorderProfile;
import android.util.Log;

public class CameraFactory {

    public static String TAG = CameraFactory.class.getSimpleName();

    private static Context sContext;

    public static void init(Context context) {
        sContext = context;
    }

    public static void openCamera(int lensFacing, CameraDevice.StateCallback callback) throws RecorderException {

        CameraManager manager = (CameraManager) sContext.getSystemService(Context.CAMERA_SERVICE);

        String cameraId = getCameraId(manager, lensFacing);

        if (cameraId == null) {
            throw new RecorderException(RecorderError.PLACEHOLDER);
        } else {
            try {
                Prefs.setCameraId(cameraId);

                boolean hasProfile = CamcorderProfile.hasProfile(
                        Integer.parseInt(Prefs.getCameraId()),
                        Prefs.getCamcorderProfile());

                if (!hasProfile) {
                    throw new RecorderException(RecorderError.UNSUPPORTED_PROFILE);
                }

                manager.openCamera(cameraId, callback, null);
            } catch (CameraAccessException e) {
                Log.e(TAG, "CameraAccessException: " + e.getMessage());
                throw new RecorderException(RecorderError.PLACEHOLDER);
            }
        }
    }

    private static String getCameraId(CameraManager manager, int lensFacing) throws RecorderException {

        String cameraId = null;

        try {
            CameraCharacteristics characteristics;
            String[] idList = manager.getCameraIdList();

            for (final String currentCameraId : idList) {
                characteristics = manager.getCameraCharacteristics(currentCameraId);
                int orientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (orientation == lensFacing) {
                    cameraId = currentCameraId;
                    int sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                    Prefs.setSensorOrientation(sensorOrientation);
                }
            }

            return cameraId;
        } catch (CameraAccessException e) {
            Log.e(TAG, "CameraAccessException: " + e.getMessage());
            throw new RecorderException(RecorderError.PLACEHOLDER);
        } catch (NullPointerException e) {
            Log.e(TAG, "CameraAccessException: " + e.getMessage());
            throw new RecorderException(RecorderError.PLACEHOLDER);
        }
    }
}
