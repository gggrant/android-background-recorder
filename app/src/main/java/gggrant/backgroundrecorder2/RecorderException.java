package gggrant.backgroundrecorder2;

public class RecorderException extends Exception {

    private RecorderError mError;

    public RecorderException(RecorderError error) {
        super();
        mError = error;
    }

    public RecorderError getError() {
        return mError;
    }

}
