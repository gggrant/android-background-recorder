package gggrant.backgroundrecorder2;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuInflater;


public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        displayEditTextSummary(R.string.pref_filename_key);
        displayEditTextSummary(R.string.pref_uploadRoute_key);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    private void displayEditTextSummary(int res) {
        EditTextPreference preference = (EditTextPreference)findPreference(Res.getString(res));
        preference.setOnPreferenceChangeListener(mEditTextListener);
        preference.setSummary(preference.getText());
    }

    private Preference.OnPreferenceChangeListener mEditTextListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            preference.setSummary((String) newValue);
            return true;
        }
    };
}
