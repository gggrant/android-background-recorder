package gggrant.backgroundrecorder2;

import android.content.Context;
import android.content.res.Resources;

import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

public class Res {

    private static Context sContext;

    public static void init(Context c) {
        sContext = c;
    }

    public static String getString(int id) {
        return sContext.getResources().getString(id);
    }

    public static boolean getBoolean(int id) {
        return sContext.getResources().getBoolean(id);
    }

    public static int getInteger(int id) {
        return sContext.getResources().getInteger(id);
    }

    //TODO: redo this
    public static String getRawString(int id) {

        String result = null;
        InputStream in_s = null;

        try {
            Resources res = sContext.getResources();
            in_s = res.openRawResource(id);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            result = new String(b);
            in_s.close();
        } catch (Exception e) {
            //TODO:
        }

        return result;
    }

    public static Certificate getRawCertificate(int id) {

        Certificate certificate = null;

        try {

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = sContext.getResources().openRawResource(id);

            certificate = cf.generateCertificate(caInput);
            caInput.close();
        } catch (Exception e) {
            //TODO:
        }

        return certificate;
    }
}
