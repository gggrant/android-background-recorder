package gggrant.backgroundrecorder2;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private Recorder mRecorder;
    private MenuItem mSettingsMenuItem;
    private MenuItem mMainMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecorder = Recorder.get();

        getFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, new MainActivityFragment(), null)
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getFragmentManager().removeOnBackStackChangedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mRecorder.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isFinishing()) {
            mRecorder.onPause(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mSettingsMenuItem = menu.findItem(R.id.action_settings);
        mMainMenuItem = menu.findItem(R.id.action_main);

        updateMenuItems();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            showSettings();
            return true;
        } else if (id == R.id.action_main) {
            hideSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isSettingsOpen()) {
            hideSettings();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackStackChanged() {
        updateMenuItems();
    }

    private boolean isSettingsOpen() {
        return getFragmentManager().getBackStackEntryCount() > 0;
    }

    private void showSettings() {

        if (Recorder.get().isReadyToRecord()) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, new SettingsFragment())
                    .addToBackStack(null)
                    .commit();
        } else {
            Toast.makeText(this, R.string.settings_busy_notice, Toast.LENGTH_LONG).show();
        }
    }

    private void hideSettings() {
        getFragmentManager().popBackStack();
    }

    private void updateMenuItems() {
        if (isSettingsOpen()) {
            mSettingsMenuItem.setVisible(false);
            mMainMenuItem.setVisible(true);
        } else {
            mSettingsMenuItem.setVisible(true);
            mMainMenuItem.setVisible(false);
        }
    }
}
