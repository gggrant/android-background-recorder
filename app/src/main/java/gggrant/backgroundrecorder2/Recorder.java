package gggrant.backgroundrecorder2;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

public class Recorder implements RecorderListener {

    private static Recorder sRecorder;

    public static Recorder get() {
        if (sRecorder == null) {
            sRecorder = new Recorder();
        }

        return sRecorder;
    }

    private RecorderState mState = RecorderState.SERVICE_NOT_RUNNING;
    private RecorderService mService;
    private List<RecorderListener> mListeners = new ArrayList<RecorderListener>();

    public RecorderState getState() {
        if (mState == RecorderState.SERVICE_RUNNING) {
            return mService.getState();
        } else {
            return mState;
        }
    }

    public boolean isReadyToRecord() {
        return getState() == RecorderState.READY_TO_RECORD;
    }

    public boolean isRecording() {
        return getState() == RecorderState.RECORDING;
    }

    public long getStartTime() {
        if (mState == RecorderState.SERVICE_RUNNING) {
            return mService.getStartTime();
        } else {
            return 0;
        }
    }

    public long getEndTime() {
        if (mState == RecorderState.SERVICE_RUNNING) {
            return mService.getEndTime();
        } else {
            return 0;
        }
    }

    public void onResume(Context context) {
        //TODO: what if mState == SERVICE_STOPPING?
        if (mState == RecorderState.SERVICE_NOT_RUNNING) {
            context.startService(new Intent(context, RecorderService.class));
        }
    }

    public void onPause(Context context) {
        if (mState == RecorderState.SERVICE_RUNNING) {
            context.stopService(new Intent(context, RecorderService.class));
        }
    }

    public void onServiceStarted(RecorderService s) {
        mService = s;
        mState = RecorderState.SERVICE_RUNNING;
    }

    public void onServiceStopped() {
        mService = null;
        mState = RecorderState.SERVICE_NOT_RUNNING;
    }

    public void addListener(RecorderListener l) {
        if (!mListeners.contains(l)) {
            mListeners.add(l);
        }
    }

    public void removeListener(RecorderListener l) {
        mListeners.remove(l);
    }

    public void startRecordingFrontFacing() {
        if (isReadyToRecord()) {
            mService.startRecordingFrontFacing();
        }
    }

    public void startRecordingRearFacing() {
        if (isReadyToRecord()) {
            mService.startRecordingRearFacing();
        }
    }

    public void stopRecording() {
        if (isRecording()) {
            mService.stopRecording();
        }
    }

    @Override
    public void onStateChanged(RecorderState state) {
        for (RecorderListener l : mListeners) {
            l.onStateChanged(state);
        }
    }

    @Override
    public void onEvent(RecorderEvent event) {
        for (RecorderListener l : mListeners) {
            l.onEvent(event);
        }
    }

    @Override
    public void onError(RecorderError error) {
        for (RecorderListener l : mListeners) {
            l.onError(error);
        }
    }
}
