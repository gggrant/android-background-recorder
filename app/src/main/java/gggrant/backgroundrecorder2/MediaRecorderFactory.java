package gggrant.backgroundrecorder2;

import android.content.Context;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.SparseIntArray;
import android.view.Surface;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MediaRecorderFactory {

    private static Context sContext;

    private static SimpleDateFormat mTimestampFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss"); //:TODO:rename

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    public static void init(Context context) {
        sContext = context;
    }

    public static MediaRecorder get() {

        MediaRecorder recorder = new MediaRecorder();

        set(recorder);

        return recorder;
    }

    public static void set(MediaRecorder recorder) {

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);//TODO: user setting?
        recorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);//TODO: user setting?
        CamcorderProfile profile = CamcorderProfile.get(Prefs.getCamcorderProfile());
        recorder.setOutputFormat(profile.fileFormat);//TODO: user setting?
        recorder.setVideoFrameRate(profile.videoFrameRate);
        recorder.setVideoSize(profile.videoFrameWidth, profile.videoFrameHeight);
        recorder.setVideoEncodingBitRate(profile.videoBitRate);
        recorder.setAudioEncoder(profile.audioCodec);
        recorder.setVideoEncoder(profile.videoCodec);//TODO: user setting?
        recorder.setOrientationHint(getOrientationHint());
        recorder.setOutputFile(getOutputFile(profile.fileFormat));
    }

    private static String getOutputFile(int fileFormat) {

        String path = sContext.getExternalFilesDir(null).getAbsolutePath() + "/";

        String saveFolder = Prefs.getSaveFolder();
        if (saveFolder.contentEquals(Res.getString(R.string.pref_saveFolder_value_app))) {
            path = sContext.getExternalFilesDir(null).getAbsolutePath() + "/";
        } else if (saveFolder.contentEquals(Res.getString(R.string.pref_saveFolder_value_dcim))) {
            path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                    .getAbsolutePath() + "/Camera/";
        } else if (saveFolder.contentEquals(Res.getString(R.string.pref_saveFolder_value_movies))) {
            path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
                    .getAbsolutePath() + "/";
        } else if (saveFolder.contentEquals(Res.getString(R.string.pref_saveFolder_value_downloads))) {
            path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .getAbsolutePath() + "/";
        }

        String name = Prefs.getFilename();
        if (Prefs.useTimestamp()) {
            name += mTimestampFormatter.format(new Date());
        }

        String extension = ".";
        switch (fileFormat) {
            case MediaRecorder.OutputFormat.MPEG_4:
                extension += "mp4";
                break;
            case MediaRecorder.OutputFormat.THREE_GPP:
                extension += "3gp";
                break;
            case MediaRecorder.OutputFormat.WEBM:
                extension += "webm";
                break;
        }

        String outputFile = path + name + extension;
        Prefs.setCurrentFilePath(outputFile);
        return outputFile;
    }

    private static int getOrientationHint() {

        int orientationHintValue = 0;

        int mSensorOrientation = Prefs.getSensorOrientation(); // camera property

        int orientationHintPref = Prefs.getOrientationHint();

        if (orientationHintPref < 0) { // auto orientation

            int rotation = Prefs.getRotation();

            switch (mSensorOrientation) {
                case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                    orientationHintValue = DEFAULT_ORIENTATIONS.get(rotation);
                    break;
                case SENSOR_ORIENTATION_INVERSE_DEGREES:
                    orientationHintValue = INVERSE_ORIENTATIONS.get(rotation);
                    break;
            }

        } else { // all manual options

            switch (mSensorOrientation) {
                case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                    orientationHintValue = orientationHintPref;
                    break;
                case SENSOR_ORIENTATION_INVERSE_DEGREES:
                    orientationHintValue = (orientationHintPref + 180) % 360;
                    break;
            }
        }

        return orientationHintValue;
    }
}
