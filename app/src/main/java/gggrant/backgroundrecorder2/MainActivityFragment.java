package gggrant.backgroundrecorder2;

import android.app.Fragment;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityFragment extends Fragment implements RecorderListener {

    public static String TAG = MainActivityFragment.class.getSimpleName();

    private static int SECONDS_PER_HOUR = 3600;
    private static int SECONDS_PER_MINUTE = 60;
    private static int MILLISECONDS_PER_SECOND = 1000;

    private Recorder mRecorder;
    private View mRootView;
    private Button mFrontFacingButton, mBackFacingButton, mStopButton;
    private TextView mStatusText, mTimerText;

    private Handler mHandler = new Handler();
    private Runnable mUpdateStatusTimerRunnable = new Runnable() {
        public void run() {

            updateStatusText();
            updateTimer();

            RecorderState state = mRecorder.getState();

            mHandler.removeCallbacksAndMessages(null);

            if (state == RecorderState.RECORDING ||
                    state == RecorderState.CAMERA_OPENING ||
                    state == RecorderState.SESSION_CONFIGURING ||
                    state == RecorderState.CAMERA_CLOSING) {

                mHandler.postDelayed(this, 1000);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);

        mRecorder = Recorder.get();

        mStatusText = (TextView) mRootView.findViewById(R.id.status);
        mTimerText = (TextView) mRootView.findViewById(R.id.timer);

        mFrontFacingButton = (Button) mRootView.findViewById(R.id.front_facing);
        mFrontFacingButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setRotation();
                mRecorder.startRecordingFrontFacing();
            }
        });

        mBackFacingButton = (Button) mRootView.findViewById(R.id.back_facing);
        mBackFacingButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                setRotation();
                mRecorder.startRecordingRearFacing();
            }
        });

        mStopButton = (Button) mRootView.findViewById(R.id.stop);
        mStopButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                mRecorder.stopRecording();
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mRecorder.addListener(this);
        updateLayout();
    }

    @Override
    public void onPause() {
        super.onPause();
        mRecorder.removeListener(this);
    }

    @Override
    public void onStateChanged(RecorderState state) {
        Log.d(TAG, "onStateChanged: " + state.name());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateLayout();
            }
        });
    }

    @Override
    public void onEvent(final RecorderEvent event) {
        Log.d(TAG, "onEvent: " + event.name());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (event) {
                    case RECORDING_FINISHED:
                        String toastText = Res.getString(R.string.toast_file_saved) + " " + Prefs.getCurrentFilePath();
                        Toast.makeText(getActivity(), toastText, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onError(final RecorderError error) {
        Log.d(TAG, "onError: " + error.name());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String message;

                switch (error) {
                    case UNSUPPORTED_PROFILE:
                        message = Res.getString(R.string.error_unsupported_profile);
                        break;

                    //TODO: replace with specific errors
                    default:
                        message = "Error";
                        break;
                }

                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateLayout() {

        switch (mRecorder.getState()) {
            case SERVICE_NOT_RUNNING:
            case SERVICE_STARTING:
            case SERVICE_STOPPNG:
            case READY_TO_RECORD:
                mStatusText.setVisibility(View.GONE);
                mTimerText.setVisibility(View.GONE);
                mFrontFacingButton.setVisibility(View.VISIBLE);
                mBackFacingButton.setVisibility(View.VISIBLE);
                mStopButton.setVisibility(View.GONE);
                break;

            case CAMERA_OPENING:
            case SESSION_CONFIGURING:
            case RECORDING:
            case CAMERA_CLOSING:
                mStatusText.setVisibility(View.VISIBLE);
                mTimerText.setVisibility(View.VISIBLE);
                mFrontFacingButton.setVisibility(View.GONE);
                mBackFacingButton.setVisibility(View.GONE);
                mStopButton.setVisibility(View.VISIBLE);
                break;
        }

        switch (mRecorder.getState()) {

            case READY_TO_RECORD:
            case RECORDING:
                mTimerText.setEnabled(true);
                mFrontFacingButton.setEnabled(true);
                mBackFacingButton.setEnabled(true);
                mStopButton.setEnabled(true);
                break;
            case SERVICE_NOT_RUNNING:
            case SERVICE_STARTING:
            case SERVICE_STOPPNG:
            case CAMERA_OPENING:
            case SESSION_CONFIGURING:
            case CAMERA_CLOSING:
                mTimerText.setEnabled(false);
                mFrontFacingButton.setEnabled(false);
                mBackFacingButton.setEnabled(false);
                mStopButton.setEnabled(false);
                break;
        }

        mHandler.post(mUpdateStatusTimerRunnable);
    }

    private void updateStatusText() {

        String text = "";

        switch (mRecorder.getState()) {

            case RECORDING:
                text = Res.getString(R.string.status_recording) + " <b>";
                switch (Prefs.getLensFacing()) {
                    case CameraCharacteristics.LENS_FACING_FRONT:
                        text += Res.getString(R.string.status_front_facing_camera);
                        break;
                    case CameraCharacteristics.LENS_FACING_BACK:
                        text += Res.getString(R.string.status_rear_facing_camera);
                        break;
                }
                text += "</b>";
                break;
            case CAMERA_OPENING:
            case SESSION_CONFIGURING:
                text = Res.getString(R.string.status_initializing);
                break;
            case CAMERA_CLOSING:
                text = Res.getString(R.string.status_finalizing);
                break;
        }

        long dots = getSecondsFromStart() % 4; //TODO: comment
        for (int i = 0; i < 4; i++) {
            if (i < dots) {
                text += "&nbsp;.";
            } else {
                text += "&nbsp;&nbsp;";
            }
        }

        mStatusText.setText(Html.fromHtml(text));
    }

    private void updateTimer() {

        int durationLimitSeconds = Prefs.getDurationLimit() / MILLISECONDS_PER_SECOND;

        switch (mRecorder.getState()) {
            case RECORDING:
                if (durationLimitSeconds == 0) {
                    mTimerText.setText(formatTimerText(getSecondsFromStart()));
                } else {
                    mTimerText.setText(formatTimerText(
                            getSecondsApart(mRecorder.getStartTime(),
                                    System.currentTimeMillis()) - durationLimitSeconds));
                }
                break;
            case CAMERA_CLOSING:
                if (durationLimitSeconds == 0) {
                    mTimerText.setText(formatTimerText(getSecondsApart(
                            mRecorder.getStartTime(), mRecorder.getEndTime())));
                } else {
                    mTimerText.setText(formatTimerText(
                            getSecondsApart(mRecorder.getStartTime(),
                                    mRecorder.getEndTime()) - durationLimitSeconds));
                }
                break;

            case CAMERA_OPENING:
            case SESSION_CONFIGURING:
                mTimerText.setText(formatTimerText(0));
                break;
        }
    }

    private String formatTimerText(long totalSeconds) {
        long absTotalSeconds = Math.abs(totalSeconds);
        long hours = absTotalSeconds / SECONDS_PER_HOUR;
        long minutes = (absTotalSeconds % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE;
        long seconds = (absTotalSeconds % SECONDS_PER_MINUTE);

        String timerText = String.format("%02d", hours) + ":"
                        + String.format("%02d", minutes) + ":"
                        + String.format("%02d", seconds);
        if (totalSeconds < 0) {
            timerText = "-" + timerText;
        }
        return timerText;
    }

    private long getSecondsApart(long startTimeMs, long endTimeMs) {
        long durationMs = endTimeMs - startTimeMs;

        return Math.max(durationMs / MILLISECONDS_PER_SECOND, 0);
    }

    private long getSecondsFromStart() {
        long startTimeMs = mRecorder.getStartTime();
        long endTimeMs = System.currentTimeMillis();

        return getSecondsApart(startTimeMs, endTimeMs);
    }

    private void setRotation() {
        Prefs.setRotation(getActivity().getWindowManager().getDefaultDisplay().getRotation());
    }
}
