package gggrant.backgroundrecorder2;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class Prefs {

    private static SharedPreferences sSharedPreferences;

    public static void init(Context c) {
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
    }

    public static int getCamcorderProfile() {
        return getInt(R.string.pref_camcorderProfile_key, R.string.pref_camcorderProfile_default);
    }

    public static int getOrientationHint() {
        return getInt(R.string.pref_orientationHint_key, R.string.pref_orientationHint_default);
    }

    public static String getSaveFolder() {
        return getString(R.string.pref_saveFolder_key, R.string.pref_saveFolder_default);
    }

    public static String getFilename() {
        return getString(R.string.pref_filename_key, R.string.pref_filename_default);
    }

    public static boolean useTimestamp() {
        return getBoolean(R.string.pref_timestamp_key, R.string.pref_timestamp_default);
    }

    public static int getDurationLimit() {
        return getInt(R.string.pref_durationLimit_key, R.string.pref_durationLimit_default);
    }

    public static int getSplitDuration() {
        return getInt(R.string.pref_splitDuration_key, R.string.pref_splitDuration_default);
    }

    public static String getUploadRoute() {
        return getString(R.string.pref_uploadRoute_key, R.string.pref_uploadRoute_default);
    }

    public static boolean getEnableUpload() {
        return getBoolean(R.string.pref_enableUpload_key, R.string.pref_enableUpload_default);
    }

    // not visible to users
    public static String getCameraId() {
        return getString(R.string.pref_cameraId_key, R.string.pref_cameraId_default);
    }
    public static void setCameraId(String value) {
        setString(R.string.pref_cameraId_key, value);
    }

    public static int getLensFacing() {
        return getInt(R.string.pref_lensFacing_key, R.string.pref_lensFacing_default);
    }
    public static void setLensFacing(int value) {
        setInt(R.string.pref_lensFacing_key, value);
    }

    public static int getRotation() {
        return getInt(R.string.pref_rotation_key, R.string.pref_rotation_default);
    }
    public static void setRotation(int value) {
        setInt(R.string.pref_rotation_key, value);
    }

    public static int getSensorOrientation() {
        return getInt(R.string.pref_sensorOrientation_key, R.string.pref_orientationHint_default);
    }
    public static void setSensorOrientation(int value) {
        setInt(R.string.pref_sensorOrientation_key, value);
    }

    public static String getCurrentFilePath() {
        return getString(R.string.pref_currentFilePath_key, R.string.pref_currentFilePath_default);
    }
    public static void setCurrentFilePath(String value) {
        setString(R.string.pref_currentFilePath_key, value);
    }




    public static String getString(int keyId, int defaultId) {
        String key = Res.getString(keyId);
        String defaultValue = Res.getString(defaultId);
        return sSharedPreferences.getString(key, defaultValue);
    }

    public static void setString(int keyId, String value) {
        String key = Res.getString(keyId);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static int getInt(int keyId, int defaultId) {
        String key = Res.getString(keyId);
        String defaultValue = Res.getString(defaultId);
        return Integer.parseInt(sSharedPreferences.getString(key, defaultValue));
    }

    public static void setInt(int keyId, int value) {
        String key = Res.getString(keyId);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, String.valueOf(value));
        editor.commit();
    }

    public static boolean getBoolean(int keyId, int defaultId) {
        String key = Res.getString(keyId);
        boolean defaultValue = Boolean.valueOf(Res.getString(defaultId));
        return sSharedPreferences.getBoolean(key, defaultValue);
    }

    public static void setBoolean(int keyId, boolean value) {
        String key = Res.getString(keyId);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }
}
