package gggrant.backgroundrecorder2;

public interface RecorderListener {

    public void onStateChanged(RecorderState state);
    public void onEvent(RecorderEvent event);
    public void onError(RecorderError error);

}
