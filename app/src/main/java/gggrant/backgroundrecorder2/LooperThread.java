package gggrant.backgroundrecorder2;

import android.os.Handler;
import android.os.Looper;

public class LooperThread extends Thread {
    private Handler mHandler;

    public void run() {
        Looper.prepare();
        mHandler = new Handler();
        Looper.loop();
    }

    public Handler getHandler() {
        return mHandler;
    }
}
