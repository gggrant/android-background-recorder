package gggrant.backgroundrecorder2;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class PrefsTest {

    @Before
    public void setUp() {
        Prefs.init(InstrumentationRegistry.getContext());
    }

    @Test
    public void getSetCurrentFilePath() {
        String filePath = "/some/file/path/here.jpg";
        Prefs.setCurrentFilePath(filePath);
        String returnedFilePath = Prefs.getCurrentFilePath();

        assertThat(returnedFilePath, is(filePath));
    }

    @Test
    public void getSetRotation() {
        int rotation = 87;
        Prefs.setRotation(rotation);
        int returnedRotation = Prefs.getRotation();

        assertThat(returnedRotation, is(rotation));
    }

    @Test
    public void getSetEnableUpload() {
        boolean enableUpload = true;
        Prefs.setBoolean(R.string.pref_enableUpload_key, enableUpload);
        boolean returnedEnableUpload = Prefs.getEnableUpload();

        assertThat(returnedEnableUpload, is(enableUpload));
    }
}
