package gggrant.backgroundrecorder2;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.text.InputType;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isFocusable;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withInputType;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void startStopRecording() {

        onView(withId(R.id.front_facing)).perform(click());

        checkIsNotDisplayed(R.id.front_facing);
        checkIsNotDisplayed(R.id.back_facing);
        checkIsDisplayed(R.id.stop);
        checkIsDisplayed(R.id.status);
        checkIsDisplayed(R.id.timer);

        onView(withId(R.id.stop)).perform(click());

        checkIsNotDisplayed(R.id.stop);
        checkIsNotDisplayed(R.id.status);
        checkIsNotDisplayed(R.id.timer);
        checkIsDisplayed(R.id.front_facing);
        checkIsDisplayed(R.id.back_facing);
    }

    @Test
    public void showHideSettings() {

        //TODO: stricter checks

        onView(withId(R.id.action_settings)).perform(click());

        onView(withText(R.string.prefCategpry_video)).check(matches(isDisplayed()));

        onView(withId(R.id.action_main)).perform(click());

        checkIsDisplayed(R.id.front_facing);
        checkIsDisplayed(R.id.back_facing);
    }

    @Test
    public void setFilename() {

        String filename = "somefilename.gif";

        onView(withId(R.id.action_settings)).perform(click());

        onView(withText(R.string.pref_filename_title)).perform(click());

        //TODO: locate text input
        //onView(withInputType(InputType.TYPE_NULL)).perform(replaceText(filename));

    }

    private void checkIsDisplayed(int id) {
        onView(withId(id)).check(matches(isDisplayed()));
    }

    private void checkIsNotDisplayed(int id) {
        onView(withId(id)).check(matches(not(isDisplayed())));

    }

}


