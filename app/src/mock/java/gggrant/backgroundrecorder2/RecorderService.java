package gggrant.backgroundrecorder2;

import android.app.Service;
import android.content.Intent;
import android.hardware.camera2.CameraCharacteristics;
import android.os.IBinder;

public class RecorderService extends Service {

    public static String TAG = RecorderService.class.getSimpleName();

    private Recorder mRecorder;
    private RecorderState mState;
    private long mStartTime, mEndTime;
    private LooperThread mLooperThread = new LooperThread();

    @Override
    public void onCreate() {
        super.onCreate();
        mLooperThread.start();

        mRecorder = Recorder.get();

        mRecorder.onServiceStarted(this);
        setState(RecorderState.READY_TO_RECORD);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLooperThread.interrupt();

        mRecorder.onServiceStopped();
        setState(RecorderState.SERVICE_NOT_RUNNING);
    }

    public RecorderState getState() {
        return mState;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public long getEndTime() {
        return mEndTime;
    }

    public void startRecordingFrontFacing() {
        startRecording(CameraCharacteristics.LENS_FACING_FRONT);
    }

    public void startRecordingRearFacing() {
        startRecording(CameraCharacteristics.LENS_FACING_BACK);
    }

    public void startRecording(final int lensFacing) {

        if (mState == RecorderState.READY_TO_RECORD) {

            mLooperThread.getHandler().post(new Runnable() {

                @Override
                public void run() {
                    setState(RecorderState.CAMERA_OPENING);
                    Prefs.setLensFacing(lensFacing);
                    Prefs.setCameraId("0");
                    Prefs.setSensorOrientation(0);
                    Prefs.setCurrentFilePath("/path/to/some/file.gif");
                    markStartTime();
                    //TODO: timer for setState(RecorderState.CAMERA_OPENING);
                    setState(RecorderState.RECORDING);

                    if (Prefs.getDurationLimit() > 0) {

                        mLooperThread.getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                stopRecording();
                            }
                        }, Prefs.getDurationLimit());
                    }
                }
            });
        }
    }

    public void stopRecording() {

        if (mState == RecorderState.RECORDING) {

            //TODO: timer for setState(RecorderState.CAMERA_CLOSING);

            mLooperThread.getHandler().post(new Runnable() {

                @Override
                public void run() {
                    clearLooper();
                    markEndTime();
                    setState(RecorderState.READY_TO_RECORD);
                    mRecorder.onEvent(RecorderEvent.RECORDING_FINISHED);
                }
            });
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void markStartTime() {
        mStartTime = System.currentTimeMillis();
    }

    private void markEndTime() {
        mEndTime = System.currentTimeMillis();
    }

    private void setState(RecorderState state) {
        mState = state;
        notifyStateChange();
    }

    private void notifyStateChange() {
        mRecorder.onStateChanged(mState);
    }

    private void clearLooper() {
        mLooperThread.getHandler().removeCallbacksAndMessages(null);
    }
}
