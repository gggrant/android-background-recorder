package gggrant.backgroundrecorder2;

import android.app.Service;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;

public class RecorderService extends Service implements MediaRecorder.OnErrorListener {

    public static String TAG = RecorderService.class.getSimpleName();

    private Recorder mRecorder;
    private RecorderState mState, mPreviousState;
    private long mStartTime, mEndTime;
    private boolean mFirstSplitFile;
    private String mLastCompletedFilePath;
    private CameraDevice mCameraDevice;
    private MediaRecorder mMediaRecorder, mPreviousMediaRecorder;
    private CameraCaptureSession mCaptureSession;
    private CaptureRequest.Builder mCaptureRequestBuilder;
    private LooperThread mLooperThread = new LooperThread();

    private CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            onCameraOpened(cameraDevice);
        }

        @Override
        public void onClosed(CameraDevice cameraDevice) {
            onCameraClosed(cameraDevice);
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            onCameraDisconnected(cameraDevice);
        }

        @Override
        public void onError(CameraDevice cameraDevice, int i) {
            onCameraError(cameraDevice, i);
        }
    };

    private CameraCaptureSession.StateCallback mCameraCaptureSessionStateCallback = new CameraCaptureSession.StateCallback() {

        @Override
        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
            onCaptureSessionConfigured(cameraCaptureSession);
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
            onCaptureSessionConfigureFailed(cameraCaptureSession);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mLooperThread.start();

        mRecorder = Recorder.get();

        mRecorder.onServiceStarted(this);
        setState(RecorderState.READY_TO_RECORD);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLooperThread.interrupt();

        mRecorder.onServiceStopped();
        setState(RecorderState.SERVICE_NOT_RUNNING);
    }

    public RecorderState getState() {
        return mState;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public long getEndTime() {
        return mEndTime;
    }

    public void startRecordingFrontFacing() {
        startRecording(CameraCharacteristics.LENS_FACING_FRONT);
    }

    public void startRecordingRearFacing() {
        startRecording(CameraCharacteristics.LENS_FACING_BACK);
    }

    public void startRecording(final int lensFacing) {

        if (mState == RecorderState.READY_TO_RECORD) {

            mLooperThread.getHandler().post(new Runnable() {

                @Override
                public void run() {

                    try {
                        setState(RecorderState.CAMERA_OPENING);
                        mFirstSplitFile = true;
                        Prefs.setLensFacing(lensFacing);
                        CameraFactory.openCamera(lensFacing, mCameraDeviceStateCallback);
                    } catch (RecorderException e) {
                        Log.e(TAG, "RecorderException: " + e.getError().name());
                        //no need to clean up
                        setState(RecorderState.READY_TO_RECORD);
                        mRecorder.onError(e.getError());
                    }
                }
            });
        }
    }

    public void stopRecording() {

        if (mState == RecorderState.RECORDING) {

            setState(RecorderState.CAMERA_CLOSING);

            mLooperThread.getHandler().post(new Runnable() {

                @Override
                public void run() {
                    clearLooper();
                    stopCaptureSession();
                    markEndTime();
                    mLastCompletedFilePath = Prefs.getCurrentFilePath();
                    uploadLastFile();
                    closeCameraDevice();
                    stopMediaRecorder();
                    releaseMediaRecorder();
                    mRecorder.onEvent(RecorderEvent.RECORDING_FINISHED);
                }
            });
        }
    }

    private void onCameraOpened(CameraDevice cameraDevice) {
        Log.d(TAG, "onCameraOpened");

        mCameraDevice = cameraDevice;
        setState(RecorderState.SESSION_CONFIGURING);
        createCaptureSession();
    }

    // CameraDevice has already been opened, need to recreate CaptureSession and MediaRecorder
    private void prepareForNextSplitFile() {

        mFirstSplitFile = false;
        mLastCompletedFilePath = Prefs.getCurrentFilePath();
        mPreviousMediaRecorder = mMediaRecorder;
        createCaptureSession();
    }

    private void createCaptureSession() {

        try {
            mMediaRecorder = MediaRecorderFactory.get();
            mMediaRecorder.setOnErrorListener(this);
            mMediaRecorder.prepare();

            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            Surface recorderSurface = mMediaRecorder.getSurface();
            surfaces.add(recorderSurface);
            mCaptureRequestBuilder.addTarget(recorderSurface);

            //TODO: stopCaptureSession(); for !mFirstSplitFile?

            mCameraDevice.createCaptureSession(surfaces, mCameraCaptureSessionStateCallback, null);

        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
            closeCameraDevice();
            mRecorder.onError(RecorderError.PLACEHOLDER);
        } catch (CameraAccessException e) {
            Log.e(TAG, "CameraAccessException: " + e.getMessage());
            releaseMediaRecorder();
            closeCameraDevice();
            mRecorder.onError(RecorderError.PLACEHOLDER);
        }
    }

    private void onCameraClosed(CameraDevice cameraDevice) {
        Log.d(TAG, "onCameraClosed");
        mCameraDevice = null;
        setState(RecorderState.READY_TO_RECORD);
    }

    private void onCameraDisconnected(CameraDevice cameraDevice) {
        Log.e(TAG, "onCameraDisconnected");
        mCameraDevice = null;
        clearLooper();
        stopMediaRecorder();
        releaseMediaRecorder();
        setState(RecorderState.READY_TO_RECORD);
        mRecorder.onError(RecorderError.PLACEHOLDER);
    }

    private void onCameraError(CameraDevice cameraDevice, int i) {
        Log.e(TAG, "onCameraError: " + i);
        mCameraDevice = null;
        clearLooper();
        stopMediaRecorder();
        releaseMediaRecorder();
        setState(RecorderState.READY_TO_RECORD);
        mRecorder.onError(RecorderError.PLACEHOLDER);
    }

    private void onCaptureSessionConfigured(CameraCaptureSession cameraCaptureSession) {
        Log.d(TAG, "onConfigured");

        try {

            mCaptureSession = cameraCaptureSession;
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            mCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, new Handler());//TODO:handler);

            if (mFirstSplitFile) {
                markStartTime();
            } else {
                stopPreviousMediaRecorder();
                releasePreviousMediaRecorder();
                uploadLastFile();
            }
            mMediaRecorder.start();
            setDelayedRunnables();

            setState(RecorderState.RECORDING);

        } catch (CameraAccessException e) {
            Log.e(TAG, "CameraAccessException: " + e);
            clearLooper();
            releaseMediaRecorder();
            closeCameraDevice();
            mRecorder.onError(RecorderError.PLACEHOLDER);
        }
    }

    private void onCaptureSessionConfigureFailed(CameraCaptureSession cameraCaptureSession) {
        Log.e(TAG, "onConfigureFailed");
        clearLooper();
        releaseMediaRecorder();
        closeCameraDevice();
        mRecorder.onError(RecorderError.PLACEHOLDER);
    }

    private void setDelayedRunnables() {

        if (Prefs.getSplitDuration() > 0) {

            mLooperThread.getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    prepareForNextSplitFile();
                }
            }, Prefs.getSplitDuration());
        }

        if (mFirstSplitFile && Prefs.getDurationLimit() > 0) {

            mLooperThread.getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    stopRecording();
                }
            }, Prefs.getDurationLimit());
        }
    }

    private void clearLooper() {
        mLooperThread.getHandler().removeCallbacksAndMessages(null);
    }

    private void stopCaptureSession() {
        if (mCaptureSession != null) {
            try {
                mCaptureSession.stopRepeating();
                mCaptureSession.abortCaptures();
                mCaptureSession = null;
            } catch (CameraAccessException e) {
                Log.e(TAG, "cleanupCaptureSession: " + e.getMessage());
            }
        }
    }

    private void stopMediaRecorder() {
        stopMediaRecorder(mMediaRecorder);
    }

    private void stopPreviousMediaRecorder() {
        stopMediaRecorder(mPreviousMediaRecorder);
    }

    private void stopMediaRecorder(MediaRecorder recorder) {
        if (recorder != null) {
            try {
                recorder.stop();
            } catch (Exception e) {
                Log.e(TAG, "stopMediaRecorder: " + e.getMessage());
            }
        }
    }

    private void releaseMediaRecorder() {
        releaseMediaRecorder(mMediaRecorder);
        mMediaRecorder = null;
    }

    private void releasePreviousMediaRecorder() {
        releaseMediaRecorder(mPreviousMediaRecorder);
        mPreviousMediaRecorder = null;
    }

    private void releaseMediaRecorder(MediaRecorder recorder) {
        if (recorder != null) {
            recorder.release();
        }
    }

    private void closeCameraDevice() {
        if (mCameraDevice != null) {
            if (mState != RecorderState.CAMERA_CLOSING) {
                setState(RecorderState.CAMERA_CLOSING);
            }
            mCameraDevice.close();
            mCameraDevice = null;
        } else {
            setState(RecorderState.READY_TO_RECORD);
            mRecorder.onEvent(RecorderEvent.RECORDING_FINISHED);
        }
    }

    private void uploadLastFile() {
        if (Prefs.getEnableUpload()) {
           FileUploader.upload(mLastCompletedFilePath);
        }
    }

    private void markStartTime() {
        mStartTime = System.currentTimeMillis();
    }

    private void markEndTime() {
        mEndTime = System.currentTimeMillis();
    }

    private void setState(RecorderState state) {
        mPreviousState = mState;
        mState = state;
        notifyStateChange();
    }

    private void notifyStateChange() {
        mRecorder.onStateChanged(mState);
    }

    @Override
    public void onError(MediaRecorder mediaRecorder, int what, int extra) {
        Log.e(TAG, "MediaRecorder.onError " + what + " - " + extra);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
